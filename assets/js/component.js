Vue.component('greeting',{
    template: `
    <div>
    <b>Hello, Vue Indonesia. . </b> <br>
    <i>Kiki, Do you love me?</i>
    </div>
    `
});

Vue.component('button-counter', {
    data: function(){
        return {
            count: 0
        }
    },
    template: '<button v-on:click="count++">You Click Me {{count}} Times</button>'
})

Vue.component('blog-posts', {
    props: ['post'],
    template: `
    <div>
    <b>{{post.title}} </b> <br>
    <p v-html="post.content"></p>
    </div>
    `
})
var app = new Vue({
    el: '#app',
    data: {
        posts: [
            {id: 1, title: 'Bowo Alpenlibe Makan Bakso Malang', content: 'isinya panjang pokoknya'},
            {id: 2, title: 'Andre Taulani Makan Semangka ', content: 'isinya panjang pokoknya hehehehe'},
            {id: 3, title: 'Young Lex Makan Bakpau Daging di Malang', content: 'isinya panjang pokoknya wkwkwkwkwk'},
        ]
    }
})