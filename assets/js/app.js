var app = new Vue({
    el: '#app',
    data: {
        handphone: [
        {type: 'Tablet', brand: 'Advan', color: 'Blue' },
        {type: 'Smartphone', brand: 'Iphone', color: 'Red' },
        {type: 'Pager', brand: 'Samsung', color: 'Green' },
        ],
        object: {
            firstName: 'John',
            lastName: 'Andarta',
            age: 27
        },
        show: false,
        message: 'Hello World!',
        message2: 'Hello Vue!',
        int1: 1,
        int2: 4,
        result: null,
        dollar: 0,
        rupiah: 14000,
        tulisan: null,
        textarea: '',
        radiobutton: '',
        select: [],
        checkbox: [],
        urlImg: 'assets/img/dp-ihwan-lite.jpg',
        link: 'https://ihwan.me',
        fontStyle: 'font-size: 20px; color: blue;',
        pesan: '',
        saran: '',
        vehicles: ['motor', 'mobil', 'sepeda'],
        names:['Ihwan', 'Andi', 'Rangga'],
        content: ''
    },
    computed:{
        sum: function(){
            return this.int1 + this.int2;
        }
    },
    methods: {
        sumProcess: function(a){
            return this.result = this.int1 + this.int2 + a;
        },
        showAlert: function(){
            alert('Hello Vue JS!');
        },
        escKey: function(){
            alert('Pasti Habis tekan ESC');
        },
        downKey: function(){
            this.saran = "tekan bawah mulu";
        },
        addName: function(){
            this.names.push(this.content);
            this.content = '';
        },
        removeName: function(index){
            this.names.splice(index, 1);
        }
    },
    watch: {
        dollar: function(val){
            this.dollar = val;
            this. rupiah = val * 14000;
        }, 
        rupiah: function(val){
            this.dollar = val / 14000;
            this.rupiah = val;
            
        },
        
    }
})